Reaktor Assignment 2020:
=======
This is a solution to the Reaktor's assignment for the a junior position in 2020.

**Assignment Instruction:** 

On a Debian and Ubuntu systems, there is a file called /var/lib/dpkg/status that holds information about software packages that the system knows about. Write a small program in a programming language of your choice that exposes some key information about packages in the file via an HTML interface.

- The index page lists installed packages alphabetically with package names as links.
- When following each link, you arrive at a piece of information about a single package. The following information should be included:

    - Name
    - Description
    - The names of the packages the current package depends on (skip version numbers)
    - The names of the packages that depend on the current package

- The dependencies and reverse dependencies should be clickable and the user can navigate the package structure by clicking from package to package.
- The application must be available publicly on the internet. You can, for example, use Heroku to host it for free. Provide a link to the website in your job application.
- The source code must also be available publicly in GitLab or GitHub, and a link provided in your job application.
- Minimize the use of external dependencies. The goal of the assignment is to view how you solve the problems with the programming language, not how well you use package managers. 🙂
- Please keep the code simple and sweet. The main design goal of this program is maintainability. We value the simplicity and readability of the code over the number of features.
- Only look at the Depends field. Ignore other fields that work kind of similarly, such as Suggests and Recommends.
- Sometimes there are alternates in a dependency list, separated by the pipe character |. When rendering such dependencies, render any alternative that maps to a package name that has an entry in the file as a link and just print the name of the package name for other packages.
- The section “Syntax of control files” of the Debian Policy Manual applies to the input data.

Make sure your solution runs on non-Debian systems as well. You can use the sample file as [mock data](https://gist.github.com/lauripiispanen/29735158335170c27297422a22b48caa).

**Database Design:**
- Recursive manay to many relationship between installed packages.
- One to many relationship between installed package and not installed packages.

    ![database design](https://i.ibb.co/C23zqFg/UML-DATABASE.jpg)

**Online presentation of the solution:**

[Heroku](https://reaktorassignment.herokuapp.com/)

**Running the App on Local Machine:**

1.  In order to run the app locally, you need to install required packages on your python using pip command.
The project runs on python 3.7 and required packages are listed in requirements.txt file.
    
2.  Run the below commands on project's root folder:

        python manage.py makemigrations package_finder
        python manage.py migrate
        python < package_finder\utils\populate_data.py
        python manage.py runserver


There are some credentials in some of the commits, they are not used 
on the production side at all, the latest commit is used for the production.
You might be asked to provide a secret key in order to run the project on local machine:
you can set it by changing the **SECRET_KEY** in the settings file located in 
the project's root directory to a random generated key.

        SECRET_KEY = os.environ.get("SECRET_KEY") --to--> SECRET_KEY = "someCharacters..."
