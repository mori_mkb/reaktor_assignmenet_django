from django.urls import path

from . import views

app_name = "package_finder"
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:package_id>/', views.detail, name='detail'),
]
