import os, django
os.environ['DJANGO_SETTINGS_MODULE'] = 'Reaktor_Assignment.settings'
django.setup()
import package_finder
from package_finder.models import Package, Dependency, AlterDependency


app_path = os.path.abspath(package_finder.__path__[0])
file_ = open(os.path.join(app_path,'debian_file/status.real'),'r',
             encoding="utf8")


def save_packages(packages_dict):

    for key, val in packages_dict.items():
        packg_name = key
        packg_description = val['Description']

        packages_instance, created = Package.objects.get_or_create(
            name=packg_name, description=packg_description)
        packages_instance.save()


def save_dependency(packages_dict):

    for key, val in packages_dict.items():
        pckg = key
        pckgs_dep = val['Depends']

        for pckg_dep in pckgs_dep:

            # Save installed dependency
            if pckg_dep in packages_dict:
                pckg_obj = Package.objects.get(name=pckg)
                pckg_dep_obj = Package.objects.get(name=pckg_dep)

                dep_inst, _ = Dependency.objects.get_or_create(
                    pack=pckg_obj, dependant=pckg_dep_obj)
                dep_inst.save()

            # Save alternative dependency
            else:
                pckg_obj = Package.objects.get(name=pckg)

                alt_dep_inst, _ = AlterDependency.objects.get_or_create(
                    name=pckg_dep, package=pckg_obj)
                alt_dep_inst.save()


def delete_redundant_pckgs(depends_list):
    # Dependency packages with the same name but different version number are
    # reduced to one package
    # input: ['python', 'python']
    # output: ['python']

    return [*set(depends_list)]


def remove_version_numbers(depends_list):
    # input: ['python (v=1.4)', 'python (v=2.7)']
    # output: ['python', 'python']

    # version number and package name are separated by a space if existed
    new_depends_list = []
    for each_package in depends_list:

        space_index = each_package.find(" ")
        if space_index != -1:
            packg_name = each_package[:space_index]
            new_depends_list.append(packg_name)
        else:
            new_depends_list.append(each_package)

    return new_depends_list


def clean_depends_line(depends_line):
    # input: 'python (v=1.4), python (v=2.7) | python (v=3.0), java'
    # output: ['python', 'java']

    depends_list = depends_line.split(", ")

    # Inside list: Split alternative packages separated by " | "
    # Outside list: Unpack sub-lists
    depends_list = [pckg
               for pckg_list in [p.split(" | ") for p in depends_list]
               for pckg in pckg_list]

    depends_list = remove_version_numbers(depends_list)
    depends_list = delete_redundant_pckgs(depends_list)

    return depends_list


def read_data(file):
    data_dict = {}
    info = {'Package': '',
            'Description': '',
            'Depends': []}

    for line in file:

        if line.startswith("Package:"):
            pckg = line[9:].strip()
            info['Package'] = pckg

        # Accepts lines that start with "Description:" or a with a space
        # Rejects lines that start with a ' /'
        if line.startswith("Description:") or (line.startswith(" ") and
                                               (line[0:2] != " /")):

            if len(info['Description']) == 0:
                description = line[13:]
                info['Description'] = description
            else:
                description = line
                info['Description'] += description

        if line.startswith("Depends:"):
            depends_str = line[9:].strip()
            depends = clean_depends_line(depends_str)
            info['Depends'] = depends

        if info['Package'] != '' and info['Description'] != '':
            data_dict[info['Package']] = info

        # Each package's paragraph is divided by an empty line
        # The Line Feed (LF) and The End of Line (EOL)
        if line in ["\n", "\r\n"]:
            info = {'Package': '',
                    'Description': '',
                    'Depends': []}

    return data_dict


if __name__ == "__main__":
    print("Loading data...")
    data = read_data(file_)
    save_packages(data)
    save_dependency(data)
