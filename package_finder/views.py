from django.shortcuts import render, get_object_or_404
from package_finder.models import Package


def index(request):

    context = {
        "packages_list": Package.objects.all()
    }

    return render(request, "index.html", context)


def detail(request, package_id):

    package_obj = get_object_or_404(Package, pk=package_id)
    depends = package_obj.depends_on.all()
    alter_depends = package_obj.alter_depends_on.all()

    context = {'package': package_obj,
               'depends': depends,
               'alter_depends': alter_depends}

    return render(request, "detail.html", context)


