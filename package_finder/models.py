from django.db import models


class Package(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True, null=True)
    depend_package = models.ManyToManyField('self', symmetrical=False,
                                            through='Dependency')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class Dependency(models.Model):
    pack = models.ForeignKey(Package, on_delete=models.CASCADE,
                             related_name='depends_on')
    dependant = models.ForeignKey(Package, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('pack', 'dependant')


class AlterDependency(models.Model):
    name = models.CharField(max_length=100, null=False)
    package = models.ForeignKey(Package, on_delete=models.CASCADE,
                                related_name='alter_depends_on')

    class Meta:
        unique_together = ('name', 'package')




