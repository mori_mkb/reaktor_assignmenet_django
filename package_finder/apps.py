from django.apps import AppConfig


class PackageFinderConfig(AppConfig):
    name = 'package_finder'
